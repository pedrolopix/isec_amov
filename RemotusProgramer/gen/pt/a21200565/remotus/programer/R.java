/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package pt.a21200565.remotus.programer;

public final class R {
    public static final class anim {
        public static final int anim_refresh=0x7f040000;
        public static final int in_animation=0x7f040001;
        public static final int out_animation=0x7f040002;
    }
    public static final class array {
        public static final int days_of_the_week=0x7f060002;
        public static final int exemplo=0x7f060000;
        public static final int headers=0x7f060001;
        public static final int months=0x7f060003;
    }
    public static final class attr {
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cancelLabel=0x7f010001;
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int okLabel=0x7f010000;
    }
    public static final class color {
        public static final int black=0x7f070003;
        public static final int blue=0x7f07000b;
        public static final int dark_blue=0x7f070005;
        public static final int green=0x7f07000a;
        public static final int grey=0x7f07000c;
        public static final int grey_black=0x7f07000e;
        public static final int grey_dark=0x7f07000d;
        public static final int grey_darkk=0x7f070000;
        public static final int grey_light=0x7f070001;
        public static final int list_header=0x7f070007;
        public static final int list_header_dark=0x7f070006;
        public static final int list_item=0x7f070008;
        public static final int red=0x7f070004;
        public static final int white=0x7f070002;
        public static final int yellow=0x7f070009;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
 Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f080000;
        public static final int activity_vertical_margin=0x7f080001;
    }
    public static final class drawable {
        public static final int btn_menu_refresh=0x7f020000;
        public static final int btn_radio_holo_dark=0x7f020001;
        public static final int btn_radio_off_disabled_focused_holo_dark=0x7f020002;
        public static final int btn_radio_off_disabled_holo_dark=0x7f020003;
        public static final int btn_radio_off_focused_holo_dark=0x7f020004;
        public static final int btn_radio_off_holo_dark=0x7f020005;
        public static final int btn_radio_off_pressed_holo_dark=0x7f020006;
        public static final int btn_radio_on_disabled_focused_holo_dark=0x7f020007;
        public static final int btn_radio_on_disabled_holo_dark=0x7f020008;
        public static final int btn_radio_on_focused_holo_dark=0x7f020009;
        public static final int btn_radio_on_holo_dark=0x7f02000a;
        public static final int btn_radio_on_pressed_holo_dark=0x7f02000b;
        public static final int content_remove=0x7f02000c;
        public static final int ic_explain_about=0x7f02000d;
        public static final int ic_find_next_holo_light=0x7f02000e;
        public static final int ic_find_previous_holo_light=0x7f02000f;
        public static final int ic_launcher=0x7f020010;
        public static final int ic_refresh=0x7f020011;
        public static final int seta_baixo_disable=0x7f020012;
        public static final int seta_baixo_normal=0x7f020013;
        public static final int seta_baixo_pressed=0x7f020014;
        public static final int seta_baixo_selector=0x7f020015;
        public static final int seta_cima_normal=0x7f020016;
        public static final int seta_cima_pressed=0x7f020017;
        public static final int seta_cima_selector=0x7f020018;
        public static final int transparent_backgound_normal=0x7f020019;
        public static final int transparent_backgound_pressed=0x7f02001a;
        public static final int transparent_background=0x7f02001b;
    }
    public static final class id {
        public static final int action_add=0x7f0c0048;
        public static final int action_cancel=0x7f0c0047;
        public static final int action_clear=0x7f0c004f;
        public static final int action_delete=0x7f0c0055;
        public static final int action_download=0x7f0c004e;
        public static final int action_edit=0x7f0c0054;
        public static final int action_mqtt_start=0x7f0c004d;
        public static final int action_mqtt_will=0x7f0c004c;
        public static final int action_publish_toggle=0x7f0c004a;
        public static final int action_publish_value=0x7f0c0049;
        public static final int action_rule_timer=0x7f0c0052;
        public static final int action_rule_topic=0x7f0c0053;
        public static final int action_save=0x7f0c0046;
        public static final int action_settings=0x7f0c0050;
        public static final int action_upload=0x7f0c004b;
        public static final int btnAddNewItem=0x7f0c000a;
        public static final int btnCron=0x7f0c0008;
        public static final int btnRemove=0x7f0c000c;
        public static final int cfgRemotusTopic=0x7f0c0045;
        public static final int checkbox=0x7f0c003e;
        public static final int chkActive=0x7f0c0002;
        public static final int chkRetainded=0x7f0c0022;
        public static final int contentTextFields=0x7f0c002e;
        public static final int dateMinus=0x7f0c003c;
        public static final int datePlus=0x7f0c0039;
        public static final int day=0x7f0c003a;
        public static final int done=0x7f0c0051;
        public static final int edCondition=0x7f0c0005;
        public static final int edDelay=0x7f0c0024;
        public static final int edName=0x7f0c0003;
        public static final int edQOS=0x7f0c0028;
        public static final int edShedulerPattern=0x7f0c0007;
        public static final int edTopic=0x7f0c0020;
        public static final int edValue=0x7f0c0026;
        public static final int edVarKey=0x7f0c002a;
        public static final int edVarValue=0x7f0c002c;
        public static final int email=0x7f0c0031;
        public static final int etTopic=0x7f0c000b;
        public static final int gridView=0x7f0c0000;
        public static final int header1=0x7f0c0011;
        public static final int header2=0x7f0c0013;
        public static final int header3=0x7f0c0015;
        public static final int header4=0x7f0c0017;
        public static final int header5=0x7f0c0019;
        public static final int llCondition=0x7f0c0004;
        public static final int llDelay=0x7f0c0023;
        public static final int llPickNumber=0x7f0c0038;
        public static final int llQoS=0x7f0c0027;
        public static final int llRetainded=0x7f0c0021;
        public static final int llShedulerPattern=0x7f0c0006;
        public static final int llTopic=0x7f0c001f;
        public static final int llTopics=0x7f0c0009;
        public static final int llValue=0x7f0c0025;
        public static final int llVarKey=0x7f0c0029;
        public static final int llVarValue=0x7f0c002b;
        public static final int mqtthost=0x7f0c0041;
        public static final int mqttpassword=0x7f0c0044;
        public static final int mqttport=0x7f0c0042;
        public static final int mqttusername=0x7f0c0043;
        public static final int okCancelBar=0x7f0c0035;
        public static final int options1=0x7f0c0012;
        public static final int options2=0x7f0c0014;
        public static final int options3=0x7f0c0016;
        public static final int options4=0x7f0c0018;
        public static final int options5=0x7f0c001a;
        public static final int orrequired1=0x7f0c0030;
        public static final int pager=0x7f0c0036;
        public static final int password1=0x7f0c0032;
        public static final int password2=0x7f0c0033;
        public static final int rbOption1=0x7f0c001b;
        public static final int rbOption2=0x7f0c001c;
        public static final int rbOption3=0x7f0c001e;
        public static final int required=0x7f0c002f;
        public static final int rgOptions=0x7f0c0037;
        public static final int scrollView=0x7f0c000f;
        public static final int scrollView1=0x7f0c0001;
        public static final int seekBar=0x7f0c001d;
        public static final int text=0x7f0c002d;
        public static final int textview=0x7f0c003d;
        public static final int tvStatus=0x7f0c0010;
        public static final int tvSubTitle=0x7f0c000e;
        public static final int tvTitle=0x7f0c000d;
        public static final int url=0x7f0c0034;
        public static final int widget_okcancelbar_cancel=0x7f0c003f;
        public static final int widget_okcancelbar_ok=0x7f0c0040;
        public static final int yearV=0x7f0c003b;
    }
    public static final class layout {
        public static final int activity_stack_over_flow=0x7f030000;
        public static final int condition=0x7f030001;
        public static final int condition_row=0x7f030002;
        public static final int crontab_header=0x7f030003;
        public static final int crontab_list=0x7f030004;
        public static final int crontab_radio_buttons=0x7f030005;
        public static final int edit_action=0x7f030006;
        public static final int edit_topic=0x7f030007;
        public static final int edit_topic_message=0x7f030008;
        public static final int edit_variable=0x7f030009;
        public static final int grid_view=0x7f03000a;
        public static final int list_item_icon_text=0x7f03000b;
        public static final int main=0x7f03000c;
        public static final int pager=0x7f03000d;
        public static final int radio_group=0x7f03000e;
        public static final int row_checkbox=0x7f03000f;
        public static final int widget_ok_cancel_bar=0x7f030010;
        public static final int x_crontab_item=0x7f030011;
    }
    public static final class menu {
        public static final int action_edit=0x7f0b0000;
        public static final int actions_list=0x7f0b0001;
        public static final int main=0x7f0b0002;
        public static final int menu_main=0x7f0b0003;
        public static final int rules_edit=0x7f0b0004;
        public static final int rules_list=0x7f0b0005;
        public static final int variable_edit=0x7f0b0006;
        public static final int variable_edit_rowselection=0x7f0b0007;
        public static final int variables=0x7f0b0008;
    }
    public static final class string {
        public static final int action_add=0x7f090039;
        public static final int action_cancel=0x7f090032;
        public static final int action_clear=0x7f090065;
        public static final int action_delete=0x7f090044;
        public static final int action_download=0x7f090064;
        public static final int action_edit=0x7f090043;
        public static final int action_mqtt_start=0x7f090060;
        public static final int action_mqtt_will=0x7f09005f;
        public static final int action_ok=0x7f090033;
        public static final int action_publish_toggle_value=0x7f09005e;
        public static final int action_publish_value=0x7f09005d;
        public static final int action_revert=0x7f090031;
        public static final int action_save=0x7f090030;
        public static final int action_settings=0x7f090001;
        public static final int actions_list_empty_text=0x7f09003d;
        public static final int app_name=0x7f090000;
        public static final int apr=0x7f090012;
        public static final int aug=0x7f090016;
        public static final int button_add_new_text=0x7f090045;
        public static final int button_delete_row_description=0x7f090046;
        public static final int day_of_month=0x7f090005;
        public static final int day_of_week=0x7f090007;
        public static final int dec=0x7f09001a;
        public static final int dialog_title_mqtt_password=0x7f09007e;
        public static final int dialog_title_mqtt_port=0x7f090078;
        public static final int dialog_title_mqtt_url=0x7f090075;
        public static final int dialog_title_mqtt_username=0x7f09007b;
        public static final int dialog_title_remotus_server=0x7f090082;
        public static final int each_selected=0x7f09001d;
        public static final int every=0x7f09001b;
        public static final int every_d=0x7f09001c;
        public static final int every_n_minutes=0x7f090002;
        public static final int feb=0x7f090010;
        public static final int fri=0x7f09000d;
        public static final int global_about=0x7f090020;
        public static final int global_accept=0x7f090028;
        public static final int global_address=0x7f090027;
        public static final int global_cancel=0x7f090021;
        public static final int global_email=0x7f090025;
        public static final int global_name=0x7f090023;
        public static final int global_password=0x7f090022;
        public static final int global_phone=0x7f090024;
        public static final int global_submit=0x7f09001f;
        public static final int global_website=0x7f090026;
        public static final int hello_world=0x7f090042;
        public static final int hint_condition=0x7f090058;
        public static final int hint_delay=0x7f09004a;
        public static final int hint_name=0x7f090051;
        public static final int hint_retainded=0x7f090050;
        public static final int hint_sheduler_pattern=0x7f09005a;
        public static final int hint_topic=0x7f090049;
        public static final int hint_value=0x7f09004e;
        public static final int hour=0x7f090004;
        public static final int jan=0x7f09000f;
        public static final int jul=0x7f090015;
        public static final int jun=0x7f090014;
        public static final int label_active=0x7f090052;
        public static final int label_alias=0x7f09005c;
        public static final int label_condition=0x7f090057;
        public static final int label_delay=0x7f09004b;
        public static final int label_name=0x7f090053;
        public static final int label_qos=0x7f09004f;
        public static final int label_retained=0x7f09004c;
        public static final int label_scheduler_pattern=0x7f090059;
        public static final int label_topic=0x7f090054;
        public static final int label_topics=0x7f09005b;
        public static final int label_value=0x7f09004d;
        public static final int mar=0x7f090011;
        public static final int may=0x7f090013;
        public static final int minute=0x7f090003;
        public static final int mon=0x7f090009;
        public static final int month=0x7f090006;
        public static final int msg_clear_all=0x7f090066;
        public static final int msg_delete_action=0x7f09006e;
        public static final int msg_delete_action_s=0x7f09006f;
        public static final int msg_delete_rule=0x7f09006c;
        public static final int msg_delete_rule_s=0x7f09006d;
        public static final int msg_delete_topic_alias=0x7f09006b;
        public static final int msg_delete_variable=0x7f090069;
        public static final int msg_delete_variable_s=0x7f09006a;
        public static final int msg_download_confirmation=0x7f090072;
        public static final int msg_mqtt_connection_error=0x7f090061;
        public static final int msg_mqtt_publish_error=0x7f090062;
        public static final int msg_mqtt_publish_ok=0x7f090063;
        public static final int msg_mqtt_subscrive_error=0x7f090071;
        public static final int msg_mqtt_subscrive_ok=0x7f090070;
        public static final int msg_please_verify_condition_values=0x7f090056;
        public static final int msg_receiving=0x7f090068;
        public static final int msg_sending=0x7f090067;
        public static final int nov=0x7f090019;
        public static final int oct=0x7f090018;
        public static final int ok=0x7f09001e;
        public static final int preference_config=0x7f090081;
        public static final int preference_mqtt=0x7f090074;
        public static final int preference_title=0x7f090073;
        public static final int qos_validor=0x7f090055;
        public static final int rules_list_empty_text=0x7f09003c;
        public static final int sat=0x7f09000e;
        public static final int sep=0x7f090017;
        public static final int summary_mqtt_password=0x7f09007f;
        public static final int summary_mqtt_port=0x7f090079;
        public static final int summary_mqtt_url=0x7f090076;
        public static final int summary_mqtt_username=0x7f09007c;
        public static final int summary_remotus_server_topic=0x7f090084;
        public static final int sun=0x7f090008;
        public static final int thu=0x7f09000c;
        public static final int title__remotus_server_topic=0x7f090083;
        public static final int title_action=0x7f090038;
        public static final int title_activity_edit_action=0x7f090041;
        public static final int title_activity_edit_rules=0x7f090040;
        public static final int title_activity_edit_topic_alias=0x7f09003f;
        public static final int title_activity_edit_variable=0x7f09003e;
        public static final int title_alias=0x7f090036;
        public static final int title_condition=0x7f090037;
        public static final int title_edit_timer_rule=0x7f090047;
        public static final int title_edit_topic_rule=0x7f090048;
        public static final int title_mqtt_port=0x7f09007a;
        public static final int title_mqtt_url=0x7f090077;
        public static final int title_mqtt_username=0x7f09007d;
        public static final int title_password=0x7f090080;
        public static final int title_rules=0x7f090034;
        public static final int title_variables=0x7f090035;
        public static final int topic_alias_list_empty_text=0x7f09003b;
        public static final int tue=0x7f09000a;
        public static final int validator_alnum=0x7f09002d;
        public static final int validator_confirm=0x7f09002c;
        public static final int validator_email=0x7f090029;
        public static final int validator_empty=0x7f09002a;
        public static final int validator_hex=0x7f09002e;
        public static final int validator_regexp=0x7f09002f;
        public static final int validator_url=0x7f09002b;
        public static final int variables_list_empty_text=0x7f09003a;
        public static final int wed=0x7f09000b;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.

    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 

        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f0a0000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
 Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f0a0001;
        public static final int ConditionHeaders=0x7f0a0004;
        public static final int RadioButton=0x7f0a0002;
        public static final int Validator_Light=0x7f0a0003;
    }
    public static final class xml {
        public static final int preferences=0x7f050000;
    }
    public static final class styleable {
        /** Attributes that can be used with a OkCancelBar.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #OkCancelBar_cancelLabel pt.a21200565.remotus.programer:cancelLabel}</code></td><td></td></tr>
           <tr><td><code>{@link #OkCancelBar_okLabel pt.a21200565.remotus.programer:okLabel}</code></td><td></td></tr>
           </table>
           @see #OkCancelBar_cancelLabel
           @see #OkCancelBar_okLabel
         */
        public static final int[] OkCancelBar = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link pt.a21200565.remotus.programer.R.attr#cancelLabel}
          attribute's value can be found in the {@link #OkCancelBar} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name pt.a21200565.remotus.programer:cancelLabel
        */
        public static final int OkCancelBar_cancelLabel = 1;
        /**
          <p>This symbol is the offset where the {@link pt.a21200565.remotus.programer.R.attr#okLabel}
          attribute's value can be found in the {@link #OkCancelBar} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name pt.a21200565.remotus.programer:okLabel
        */
        public static final int OkCancelBar_okLabel = 0;
    };
}
