/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.throrinstudio.android.example.validator;

public final class R {
	public static final class attr {
		public static final int cancelLabel = 0x7f010001;
		public static final int okLabel = 0x7f010000;
	}
	public static final class drawable {
		public static final int ic_explain_about = 0x7f02000d;
		public static final int ic_launcher = 0x7f020010;
	}
	public static final class id {
		public static final int contentTextFields = 0x7f0c002e;
		public static final int email = 0x7f0c0031;
		public static final int okCancelBar = 0x7f0c0035;
		public static final int orrequired1 = 0x7f0c0030;
		public static final int password1 = 0x7f0c0032;
		public static final int password2 = 0x7f0c0033;
		public static final int required = 0x7f0c002f;
		public static final int url = 0x7f0c0034;
		public static final int widget_okcancelbar_cancel = 0x7f0c003f;
		public static final int widget_okcancelbar_ok = 0x7f0c0040;
	}
	public static final class layout {
		public static final int main = 0x7f03000c;
		public static final int widget_ok_cancel_bar = 0x7f030010;
	}
	public static final class string {
		public static final int app_name = 0x7f090000;
		public static final int global_about = 0x7f090020;
		public static final int global_accept = 0x7f090028;
		public static final int global_address = 0x7f090027;
		public static final int global_cancel = 0x7f090021;
		public static final int global_email = 0x7f090025;
		public static final int global_name = 0x7f090023;
		public static final int global_password = 0x7f090022;
		public static final int global_phone = 0x7f090024;
		public static final int global_submit = 0x7f09001f;
		public static final int global_website = 0x7f090026;
		public static final int validator_alnum = 0x7f09002d;
		public static final int validator_confirm = 0x7f09002c;
		public static final int validator_email = 0x7f090029;
		public static final int validator_empty = 0x7f09002a;
		public static final int validator_hex = 0x7f09002e;
		public static final int validator_regexp = 0x7f09002f;
		public static final int validator_url = 0x7f09002b;
	}
	public static final class style {
		public static final int Validator_Light = 0x7f0a0003;
	}
	public static final class styleable {
		public static final int[] OkCancelBar = { 0x7f010000, 0x7f010001 };
		public static final int OkCancelBar_cancelLabel = 1;
		public static final int OkCancelBar_okLabel = 0;
	}
}
