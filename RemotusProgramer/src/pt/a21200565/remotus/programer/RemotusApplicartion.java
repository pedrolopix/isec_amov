package pt.a21200565.remotus.programer;

import pt.a21200565.remotus.programer.model.Model;
import android.app.Application;

public class RemotusApplicartion extends Application {
	@Override
	public void onCreate() {
		super.onCreate();

		Model.getInstance().load(this);
	}
}
