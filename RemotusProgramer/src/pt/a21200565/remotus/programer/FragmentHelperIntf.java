package pt.a21200565.remotus.programer;

public interface FragmentHelperIntf {
	public void onTabUnselected();

	public boolean canExit();
}
