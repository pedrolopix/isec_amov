package remotusservice.rules;

public interface Visitor {
	void accept(Visit visit);
}
