package remotusservice.rules;

import it.sauronsoftware.cron4j.Task;

public interface IEngine {

	boolean subscrive(String topic);
	public void unsubscrive(String topicFilter);
	void publish(String topic, String value, boolean retained, int qos);
	void schedule(String schedulingPattern, Task task);
	ExpressionIntf GetExpression();

}
