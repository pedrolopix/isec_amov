package remotusservice.rules;

import remotusservice.rules.actions.ActionPublish;
import remotusservice.rules.actions.ActionToggle;
import remotusservice.rules.conditions.ConditionTime;
import remotusservice.rules.conditions.ConditionTopic;


public interface Visit {
	void acceptConditionTopic(ConditionTopic condition);

	void acceptConditionTime(ConditionTime condition);
	void accept(ActionPublish action);
	void accept(ActionToggle action);
	void accept(Rules rules);
	void accept(Varibales topics);
}
