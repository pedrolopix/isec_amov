package remotusservice.rules;

import it.sauronsoftware.cron4j.Task;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import remotusservice.rules.conditions.Condition;

public class Rules implements Visitor {
	public static final ScheduledExecutorService worker =  Executors.newSingleThreadScheduledExecutor();
	List<Condition> mRules;
	Varibales mTopics;
	IEngine mModel;
	ExpressionIntf mExpression;

	public Rules(IEngine model) {
		super();
		mModel=model;
		mTopics= new Varibales();
		mRules= new ArrayList<Condition>();
		mExpression = model.GetExpression();
	}

	synchronized public void receivedTopic(String topic, String value) {
		mTopics.setVariableValue(topic, value);

		for (Condition c : mRules) {
			c.topicEvent(topic, value);
		}
	}

	synchronized public void addRule(Condition condition) {
		condition.setRules(this);
		mRules.add(condition);
	}

	public List<Condition> getRules() {
		return mRules;
	}

	public void addTopicAlias(String var, String topic) {
		mTopics.setTopicAlias(var, topic);
	}

	public void start() {
		for (Condition c : mRules) {
			c.start();
		}
	}

	public void stop() {
		for (Condition c : mRules) {
			c.stop();
		}
	}

	public void subscrive(String topic) {
		topic=mTopics.translateKey(topic);
		mModel.subscrive(topic);
	}

	public void unsubscrive(String topic) {
		topic = mTopics.translateKey(topic);
		mModel.unsubscrive(topic);
	}

	public void publish(String topic, String value, int qos,	boolean retained) {
		mModel.publish(topic, value, retained, qos);
	}

	public String calcExpression(String expression) {
		return mExpression.calc(translateVars(expression));
	}

	public String translateVars(String expression) {
		return mTopics.translateVars(expression);
	}

	public String translateKey(String key) {
		return mTopics.translateKey(key);
	}

	@Override
	public void accept(Visit visit) {
		mTopics.accept(visit);
		visit.accept(this);
		for (Condition c : mRules) {
			c.accept(visit);
		}
	}

	public Varibales getVariables() {
		return mTopics;
	}

	public void schedule(Object sender, String schedulingPattern, Task task) {
		mModel.schedule(schedulingPattern, task);
	}

	public void deleteTopic(String alias) {
		mTopics.deleteAlias(alias);
	}

	public void setVariableValue(String key, String value) {
		mTopics.setVariableValue(key, value);
	}

	public void deleteVariable(String name) {
		mTopics.deleteVariable(name);
	}

	public void deleteRule(Condition rule) {
		mRules.remove(rule);
	}

	public Condition getConditionId(int id) {
		for (Condition rule : mRules) {
			if (rule.getId() == id) {
				return rule;
			}
		}
		return null;
	}



}
