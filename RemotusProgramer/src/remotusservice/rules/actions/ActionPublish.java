package remotusservice.rules.actions;

import remotusservice.rules.Visit;

public class ActionPublish extends ActionBasePublish {

	@Override
	public void accept(Visit visit) {
		visit.accept(this);
	}

}
