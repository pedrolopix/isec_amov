package remotusservice.rules.conditions;

import it.sauronsoftware.cron4j.Task;
import it.sauronsoftware.cron4j.TaskExecutionContext;
import remotusservice.rules.Visit;
import remotusservice.rules.actions.Action;

public class ConditionTime extends Condition {
	public class MyTask extends Task {

		@Override
		public boolean canBePaused() {
			return false;
		}

		@Override
		public boolean canBeStopped() {
			return true;
		}

		@Override
		public boolean supportsCompletenessTracking() {
			return true;
		}

		@Override
		public boolean supportsStatusTracking() {
			return true;
		}

		@Override
		public void execute(TaskExecutionContext context) throws RuntimeException {
			executeAction();
		}
	}

	private String mSchedulingPattern;


	@Override
	public void topicEvent(String topic, String value) {
		//n??o implementado, este condi????o s?? interpreta timeEvents
	}

	@Override
	public void accept(Visit visit) {
		visit.acceptConditionTime(this);
		for (Action action : mActions) {
			action.accept(visit);
		}
	}

	@Override
	public void start() {
		final MyTask task = new MyTask(); // no android dá erro a criar
		if (mSchedulingPattern!=null && !mSchedulingPattern.isEmpty()) {
		    mRules.schedule(this, mSchedulingPattern,task);
		}
		super.start();
	}

	public String getSchedulingPattern() {
		return mSchedulingPattern;
	}

	public void setSchedulingPattern(String schedulingPattern) {
		this.mSchedulingPattern = schedulingPattern;
	}

}
